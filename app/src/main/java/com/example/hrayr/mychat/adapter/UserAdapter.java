package com.example.hrayr.mychat.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hrayr.mychat.ChatActivity;
import com.example.hrayr.mychat.R;
import com.example.hrayr.mychat.model.UserItem;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.List;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private List<UserItem> mUserItems;
    private List<UserItem> mAllUserItems;
    private Context mContext;
    private UserItem mCurrentUser;

    public void setAllUserItems(List<UserItem> allUserItems) {
        mAllUserItems = allUserItems;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView userName;
        ImageView userProfilePic;

        MyViewHolder(View view) {
            super(view);
            userName = view.findViewById(R.id.user_name_txt);
            userProfilePic = view.findViewById(R.id.user_imgView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            // TODO: 12/27/2017 add chat
            UserItem chatItem = getItem(getAdapterPosition());
            if (mCurrentUser != null) {
                String roomId = mAllUserItems.indexOf(mCurrentUser) > mAllUserItems.indexOf(chatItem) ? mCurrentUser.getFireBaseUid() + chatItem.getFireBaseUid() : chatItem.getFireBaseUid() + mCurrentUser.getFireBaseUid();
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("userId", mCurrentUser.getFireBaseUid());
                intent.putExtra("friendId", chatItem.getFireBaseUid());
                intent.putExtra("roomId", roomId);
                intent.putExtra("nameFriend", chatItem.getUserName());
                mContext.startActivity(intent);
            }

        }
    }

    public UserAdapter(List<UserItem> userList, UserItem currentUser) {
        mUserItems = userList;
        mCurrentUser = currentUser;
    }

    public void setUserItems(List<UserItem> userItems, UserItem currentUser) {
        mUserItems.clear();
        for (UserItem userItem : userItems) {
            if (!userItem.getFireBaseUid().equals(currentUser.getFireBaseUid())) {
                mUserItems.add(userItem);
            } else {
                mCurrentUser = userItem;
            }
        }
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.user_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        UserItem userItem = getItem(position);
        holder.userName.setText(userItem.getUserName());
        Picasso.with(mContext).load(userItem.getUserProfileImage()).into(holder.userProfilePic);
    }

    private UserItem getItem(int position) {
        return mUserItems.get(position);
    }

    @Override
    public int getItemCount() {
        return mUserItems.size();
    }
}
