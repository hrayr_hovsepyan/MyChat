package com.example.hrayr.mychat.model;

import java.util.ArrayList;

public class Conversation {
    private ArrayList<Message> mListMessageData;

    public Conversation() {
        mListMessageData = new ArrayList<>();
    }

    public ArrayList<Message> getListMessageData() {
        return mListMessageData;
    }
}