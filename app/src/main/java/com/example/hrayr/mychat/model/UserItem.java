package com.example.hrayr.mychat.model;

public class UserItem {
    private String mUserProfileImage;
    private String mUserName;
    private String mFireBaseUid;

    public String getUserProfileImage() {
        return mUserProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        mUserProfileImage = userProfileImage;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getFireBaseUid() {
        return mFireBaseUid;
    }

    public void setFireBaseUid(String fireBaseUid) {
        mFireBaseUid = fireBaseUid;
    }

}
