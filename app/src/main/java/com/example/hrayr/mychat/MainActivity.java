package com.example.hrayr.mychat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hrayr.mychat.adapter.UserAdapter;
import com.example.hrayr.mychat.model.UserItem;
import com.example.hrayr.mychat.view.DividerItemDecoration;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.TwitterAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "TwitterLogin";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public DatabaseReference mDatabase;
    private TwitterLoginButton mLoginButton;
    private UserAdapter mUserAdapter;
    private ImageView mCurrentUserProfilePic;
    private ProgressDialog mAuthProgressDialog;
    private List<UserItem> mLoggedInUser = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private TextView mUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Configure Twitter SDK
        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                getString(R.string.twitter_consumer_key),
                getString(R.string.twitter_consumer_secret));

        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();

        Twitter.initialize(twitterConfig);
        setContentView(R.layout.activity_main);
        // Views
        Button signOutBtn = findViewById(R.id.sign_out);
        mUserName = findViewById(R.id.user_name_txt);
        mCurrentUserProfilePic = findViewById(R.id.currentUser_imgView);
        mRecyclerView = findViewById(R.id.user_list);
        signOutBtn.setOnClickListener(this);
        mUserAdapter = new UserAdapter(new ArrayList<UserItem>(), null);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.recyclerview_item_divider));
        mRecyclerView.setAdapter(mUserAdapter);
        createAuthProgressDialog();

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // [START initialize_twitter_login]
        mLoginButton = findViewById(R.id.login_button);
        mLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.d(TAG, "twitterLogin:success" + result);
                handleTwitterSession(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Log.w(TAG, "twitterLogin:failure", exception);
                updateUI(null);
            }
        });
        // [END initialize_twitter_login]
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // Retrieve the user’s account data, using the getCurrentUser method//
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // If the user signs in, then display the following message//
                    Log.d(TAG, "onAuthStateChanged" + user.getUid());
                }
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (mLoggedInUser != null) {
            mLoggedInUser.clear();
        }
        updateUI(currentUser);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the Twitter login button.
        mLoginButton.onActivityResult(requestCode, resultCode, data);
    }
    // [END on_activity_result]

    // [START auth_with_twitter]
    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);
        // [START_EXCLUDE silent]
        mAuthProgressDialog.show();
        // [END_EXCLUDE]

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        if (mAuthProgressDialog != null) {
                            mAuthProgressDialog.dismiss();
                        }
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_twitter]

    private void signOut() {
        mAuth.signOut();
        TwitterCore.getInstance().getSessionManager().clearActiveSession();

        updateUI(null);
    }

    private void writeNewUser(String userId, String name, String userProfilePic) {
        UserItem user = new UserItem();
        user.setUserName(name);
        user.setUserProfileImage(userProfilePic);
        user.setFireBaseUid(userId);

        mDatabase.child("users").child(userId).setValue(user);
        dataBaseUsers(user);
    }

    //listen data changes
    private void dataBaseUsers(final UserItem userItem) {

        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    mLoggedInUser.add(ds.getValue(UserItem.class));

                }
                mUserAdapter.setUserItems(mLoggedInUser, userItem);
                mUserAdapter.setAllUserItems(mLoggedInUser);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void createAuthProgressDialog() {
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("Loading...");
        mAuthProgressDialog.setMessage("Authenticating with Firebase...");
        mAuthProgressDialog.setCancelable(false);
    }

    private void updateUI(FirebaseUser user) {

        if (user != null) {
            writeNewUser(user.getUid(), user.getDisplayName(), user.getProviderData().get(1).getPhotoUrl().toString());
            Picasso.with(this).load(user.getProviderData().get(1).getPhotoUrl().toString()).into(mCurrentUserProfilePic);
            mUserName.setText(user.getDisplayName());
            findViewById(R.id.login_button).setVisibility(View.GONE);
            mUserName.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out).setVisibility(View.VISIBLE);
            mCurrentUserProfilePic.setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.login_button).setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mUserName.setVisibility(View.GONE);
            findViewById(R.id.sign_out).setVisibility(View.GONE);
            mCurrentUserProfilePic.setVisibility(View.GONE);

        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.sign_out) {
            signOut();
        }
    }
}
